import * as myTSModule from './myTSModule'

console.log(`
  [TS] Consuming the TS module in a TS app: ${myTSModule()}
`)

export = myTSModule
